tests
testTopicos
	self assert: sistema topicos size equals: 2.
	self assert: (sistema topicos includes: top1) equals: true.
	self assert: (sistema topicos includes: top2) equals: true.
	self assert: (sistema topicos includes: top3) equals: false