tests
testTodasLasRespuestasDeUnUsuario
	self
		assert: (sistema todasLasRespuestasDeUnUsuario: usr1) size
		equals: 2.
	self
		assert: ((sistema todasLasRespuestasDeUnUsuario: usr1) includes: resp2)
		equals: true.
	self
		assert: ((sistema todasLasRespuestasDeUnUsuario: usr1) includes: resp3)
		equals: true.
	self
		assert: ((sistema todasLasRespuestasDeUnUsuario: usr1) includes: resp1)
		equals: false