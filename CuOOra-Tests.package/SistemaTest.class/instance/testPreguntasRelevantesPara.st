tests
testPreguntasRelevantesPara
	self assert: (sistema preguntasRelevantesPara: usr1) size equals: 2.
	self
		assert: ((sistema preguntasRelevantesPara: usr1) includes: preg1)
		equals: true.
	self
		assert: ((sistema preguntasRelevantesPara: usr1) includes: preg2)
		equals: true.
	self
		assert: ((sistema preguntasRelevantesPara: usr2) includes: preg1)
		equals: true.
	self
		assert: ((sistema preguntasRelevantesPara: usr2) includes: preg2)
		equals: false