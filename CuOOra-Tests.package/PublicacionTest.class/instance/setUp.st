running
setUp
	usr1 := Usuario conNombre: 'Juan' yContrasenia: '123'.
	usr2 := Usuario conNombre: 'Pedro' yContrasenia: '321'.
	top1 := Topico
		conNombre: 'Matematicas'
		yDescripcion: 'La disciplina que estudia los numeros y sus relaciones'.
	top2 := Topico
		conNombre: 'Fisica'
		yDescripcion: 'La disciplina que estudia la energia y los cuerpos'.
	top3 := Topico
		conNombre: 'Teologia'
		yDescripcion: 'La ciencia que estudia a Dios en todas sus formas'.
   topicosPreg1 := OrderedCollection with: top1.
	topicosPreg2 := OrderedCollection with: top2.
	preg1 := Pregunta
		conTitulo: 'Porque la matematica es tan dificil?'
		descripcion: ' '
		autor: usr1
		yTopicos: topicosPreg1.
	preg2 := Pregunta
		conTitulo: 'Porque la fisica es tan dificil como la matematica?'
		descripcion: ' '
		autor: usr2
		yTopicos: topicosPreg2.
	usr1 agregarPregunta: preg1.
	usr2 agregarPregunta: preg2.
	resp1 := Respuesta
		deUsuario: usr2
		pregunta: preg1
		yContenido: 'Porque tienes la cabeza llena de azerrin'.
	resp2 := Respuesta
		deUsuario: usr1
		pregunta: preg2
		yContenido: 'Porque la mayoria de la fisica es matematica'.
	resp3 := Respuesta
		deUsuario: usr1
		pregunta: preg2
		yContenido: 'La verdad ni idea'.
	preg1 agregarRespuesta: resp1.
	preg2 agregarRespuesta: resp2.
	preg2 agregarRespuesta: resp3.
