tests
testPuntaje
	preg1 darLike: usr2.
	preg2 darDislike: usr1.
	self assert: preg1 puntaje equals: 1.
	self assert: preg2 puntaje equals: -1.
	self deny: resp1 puntaje ~= 0 equals: true.
	preg2 darLike: usr2.
	self assert: preg2 puntaje equals: 0.
	preg2 darLike: usr2.
	self assert: preg2 puntaje equals: 0.
	preg1 darLike: usr1.
	self assert: preg1 puntaje equals: 2