running
setUp
	usr1 := Usuario conNombre: 'Juan' yContrasenia: '123'.
	usr2 := Usuario conNombre: 'Pedro' yContrasenia: '321'.
	top1 := Topico
		conNombre: 'Matematicas'
		yDescripcion: 'La disciplina que estudia los numeros y sus relaciones'.
	topicosPreg1 := OrderedCollection with: top1.
	preg1 := Pregunta
		conTitulo: 'Porque la matematica es tan dificil?'
		descripcion: ' '
		autor: usr1
		yTopicos: topicosPreg1 .
	usr1 agregarPregunta: preg1.
	resp1 := Respuesta
		deUsuario: usr2
		pregunta: preg1
		yContenido: 'Porque tienes la cabeza llena de azerrin'.
	
