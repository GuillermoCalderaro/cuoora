tests
testEsValido
	self assert: (usr1 esValido: 'Juan' con: '123') equals: true.
	self assert: (usr1 esValido: 'Pedro' con: '123') equals: false.
	self assert: (usr1 esValido: 'Juan' con: '321') equals: false.
	self assert: (usr1 esValido: '' con: '') equals: false