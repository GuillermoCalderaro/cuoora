as yet unclassified
esValido: unNombre con: unaContrasenia
	^ nombreDeUsuario = unNombre
		and: (self esMiContrasenia: unaContrasenia)