rendering
renderPreguntasOn: aCanvas
	aCanvas paragraph with: 'Preguntas Relevantes'.
	aCanvas anchor
		callback: [ self crearPregunta ];
		with: [ aCanvas button: 'Agregar Pregunta' ].
	aCanvas
		unorderedList:
			[ ((Sistema soleInstance preguntasRelevantesPara: self session usuario)
				takeFirst: 5)
				do: [ :each | self renderPreguntaOn: aCanvas y: each ] ]