rendering
renderPreguntaOn: aCanvas y: unaPregunta
	aCanvas
		listItem: [ aCanvas anchor
				callback: [ self verDetalleDePregunta: unaPregunta ];
				with: [ aCanvas paragraph: unaPregunta titulo ].
			unaPregunta topicos do: [ :each | aCanvas text: each nombre , ' ' ].
			aCanvas break.
			aCanvas text: 'Realizada por '.
			aCanvas anchor
				callback: [ self verPrefilDeUsuario: unaPregunta usuario ];
				with: unaPregunta usuario miNombre.
			aCanvas text: ' | '.
			aCanvas text: unaPregunta misRespuestas size.
			aCanvas text: ' Respuestas'.
			aCanvas
				paragraph:
					unaPregunta likes asString , ' likes | '
						, unaPregunta dislikes asString , ' dislikes'.
			aCanvas anchor
				callback: [ self like: unaPregunta ];
				with: 'like'.
			aCanvas space.
			aCanvas anchor
				callback: [ self dislike: unaPregunta ];
				with: 'dislike' ].
	aCanvas horizontalRule