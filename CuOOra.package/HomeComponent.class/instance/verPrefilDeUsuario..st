callbacks
verPrefilDeUsuario: unUsuario
	| perfilDeUsuario |
	perfilDeUsuario := PerfilUsuarioComponent perfilDe: unUsuario.
	self call: perfilDeUsuario