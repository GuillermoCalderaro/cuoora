callbacks
verDetalleDePregunta: unaPregunta
	| verDetalle |
	verDetalle := DetallePreguntaComponent de: unaPregunta.
	self call: verDetalle