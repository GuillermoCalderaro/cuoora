callbacks
crearPregunta
	| crearPregunta |
	crearPregunta := CrearPreguntaComponent pregunta: Pregunta new.
	self call: crearPregunta.
	crearPregunta pregunta
		ifNotNil: [ self session usuario agregarPregunta: crearPregunta pregunta ]