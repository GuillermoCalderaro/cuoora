as yet unclassified
validarUsr: unNombreDeUsuario con: unaContrasenia
	| usr |
	usr := cuoora validarUsr: unNombreDeUsuario con: unaContrasenia.
	(usr) ifNotNil:[ self iniciarSesionCon: usr].
	nombre := nil.
	contrasenia := nil