rendering
renderContentOn: aCanvas
	aCanvas paragraph with: 'CuOOra'.
	aCanvas
		form: [ aCanvas
				paragraph: [ aCanvas label: 'usuario:'.
					aCanvas textInput
						callback: [ :nom | self nombre: nom ];
						with: self nombre.
					aCanvas
						paragraph: [ aCanvas label: 'contrasena:'.
							aCanvas textInput
								callback: [ :pas | self contrasenia: pas ];
								with: self contrasenia ].
					aCanvas button
						callback: [ self validarUsr: nombre con: contrasenia ];
						with: 'Iniciar Sesion' ] ]