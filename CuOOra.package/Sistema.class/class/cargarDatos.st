as yet unclassified
cargarDatos
	| usr1 usr2 usr3 top1 top2 top3 preg1 preg2 topicosPreg1 topicosPreg2 |
	usr1 := Usuario
		conNombre: 'pedro@cuoora.com'
		yContrasenia: 'pedro@cuoora.com'.
	usr2 := Usuario
		conNombre: 'diego@cuoora.com'
		yContrasenia: 'diego@cuoora.com'.
	usr3 := Usuario
		conNombre: 'juan@cuoora.com'
		yContrasenia: 'juan@cuoora.com'.
	usr3 seguirA: usr2.
	usr1 seguirA: usr3.
	soleInstance
		agregarUsuario: usr1;
		agregarUsuario: usr2;
		agregarUsuario: usr3.
	top1 := Topico
		conNombre: 'OO1'
		yDescripcion: 'La disciplina que estudia los Objetos.'.
	top2 := Topico
		conNombre: 'Smalltalk'
		yDescripcion: 'Un lenguaje de programacion orientado a objetos'.
	top3 := Topico
		conNombre: 'Test de Unidad'
		yDescripcion: 'Area de la informatica encargada de el estudio de los test de unidad'.
	soleInstance
		agregarTopico: top1;
		agregarTopico: top2;
		agregarTopico: top3.
	topicosPreg1 := OrderedCollection with: top1 with: top3.
	topicosPreg2 := OrderedCollection with: top1 with: top2.
	preg1 := Pregunta
		conTitulo: 'Para que sirve el metodo SetUp'
		descripcion: ' '
		autor: usr1
		yTopicos: topicosPreg1.
	usr1 agregarPregunta: preg1.
	preg2 := Pregunta
		conTitulo: '¿Qué significa #messageNotUnderstood?'
		descripcion: ''
		autor: usr2
		yTopicos: topicosPreg2.
	usr2 agregarPregunta: preg2.
	preg1
		agregarRespuesta:
			(Respuesta
				deUsuario: usr2
				pregunta: preg1
				yContenido:
					'Sirve para instanciar los objetos que
son evaluados por el test en un único método y que se
ejecute siempre antes de cada test.').
	preg2
		agregarRespuesta:
			(Respuesta
				deUsuario: usr1
				pregunta: preg2
				yContenido:
					'Significa que el objeto que recibió el
mensaje no encontró ningún método para ejecutar en
respuesta')