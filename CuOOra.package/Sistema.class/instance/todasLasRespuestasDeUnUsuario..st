as yet unclassified
todasLasRespuestasDeUnUsuario: unUsuario
	| todasLasPreguntas todasLasRespuestas |
	todasLasPreguntas := usuarios
		flatCollect: [ :each | each misPreguntas ].
	todasLasRespuestas := todasLasPreguntas
		flatCollect: [ :each | each misRespuestas ].
	^ todasLasRespuestas select: [ :each | each usuario = unUsuario ]