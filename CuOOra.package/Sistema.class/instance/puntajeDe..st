as yet unclassified
puntajeDe: unUsuario
	| cantidadDePreguntas cantidadDeRespuestas todasLasPublicaciones aux |
	aux := 0.
	cantidadDePreguntas := unUsuario misPreguntas size.
	cantidadDeRespuestas := (self
		todasLasRespuestasDeUnUsuario: unUsuario) size.
	todasLasPublicaciones := unUsuario misPreguntas.
	todasLasPublicaciones
		addAll: (self todasLasRespuestasDeUnUsuario: unUsuario).
	todasLasPublicaciones do: [ :each | aux := aux + each puntaje ].
	^ cantidadDePreguntas * 20 + (cantidadDeRespuestas * 50) + aux