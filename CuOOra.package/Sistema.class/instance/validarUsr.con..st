as yet unclassified
validarUsr: unUsr con: unaContrasenia
	^ usuarios
		detect: [ :each | each esValido: unUsr con: unaContrasenia ]
		ifNone: [ ^ nil ]