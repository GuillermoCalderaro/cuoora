as yet unclassified
preguntasRelevantesPara: unUsuario
	| todasLasPreguntas |
	todasLasPreguntas := usuarios
		flatCollect: [ :each | each misPreguntas ].
	^ (todasLasPreguntas select: [ :each | unUsuario esRelevante: each ])
		asSortedCollection: [ :a :b | a fechaDeCreacion > b fechaDeCreacion ]