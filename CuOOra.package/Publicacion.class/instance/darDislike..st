as yet unclassified
darDislike: unUsuario
	reacciones
		detect: [ :each | each esMiUsuario: unUsuario ]
		ifFound: [ :reaccion | 
			reaccion esLike
				ifTrue: [ reaccion toogle ] ]
		ifNone: [ reacciones add: (Reaction unEstado: false yUsuario: unUsuario) ]