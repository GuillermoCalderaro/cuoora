as yet unclassified
darLike: unUsuario
	reacciones
		detect: [ :each | each esMiUsuario: unUsuario ]
		ifFound: [ :reaccion | 
			reaccion esLike
				ifFalse: [ reaccion toogle ] ]
		ifNone: [ reacciones add: (Reaction unEstado: true yUsuario: unUsuario) ]