rendering
renderFormaOn: aCanvas
	aCanvas
		form: [ aCanvas
				paragraph: [ aCanvas label: 'Titulo'.
					aCanvas textInput on: #titulo of: self ].
			aCanvas
				paragraph: [ aCanvas label: 'Desarrollo'.
					aCanvas textInput on: #descripcion of: self ].
			aCanvas
				paragraph: [ aCanvas label: 'Topicos'.
					aCanvas textInput on: #topicos of: self ].
			aCanvas button
				callback: [ self aceptar ];
				with: 'Guardar'.
			aCanvas space.
			aCanvas button
				callback: [ self calcelar ];
				with: 'Cancelar' ]