rendering
renderPreguntaOn: aCanvas
	aCanvas paragraph: respuesta pregunta titulo.
	 respuesta pregunta  topicos do: [ :each | aCanvas text: each nombre ].
	aCanvas break.
	aCanvas text: 'Realizada por '.
	aCanvas anchor
		callback: [ self verPrefilDeUsuario: respuesta pregunta usuario ];
		with: respuesta pregunta usuario miNombre.
	aCanvas
		paragraph:
			respuesta pregunta likes asString , ' likes | '
				, respuesta pregunta dislikes asString , ' dislikes'.
	aCanvas anchor
		callback: [ self likePregunta: respuesta pregunta ];
		with: 'like'.
	aCanvas space.
	aCanvas anchor
		callback: [ self dislikePregunta: respuesta pregunta ];
		with: 'dislike'.
	"preguntasRelevantesPara: self session usuario"