rendering
renderFormaRespuestaOn: aCanvas
	aCanvas paragraph: 'RESPONDER'.
	aCanvas
		form: [ aCanvas textArea on: #contenido of: self.
			aCanvas break.
			aCanvas button
				callback: [ self aceptar ];
				with: 'Guardar'.
			aCanvas space.
			aCanvas button
				callback: [ self cancelar ];
				with: 'Cancelar' ]