callbacks
agregarRespuesta
	| crearRespuesta |
	crearRespuesta := CrearRespuestaComponent
		respuesta:
			(Respuesta
				deUsuario: self session usuario
				pregunta: pregunta
				yContenido: ' ').
	self call: crearRespuesta.
	crearRespuesta respuesta
		ifNotNil: [ pregunta agregarRespuesta: crearRespuesta respuesta ]