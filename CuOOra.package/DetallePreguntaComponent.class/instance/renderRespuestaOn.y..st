rendering
renderRespuestaOn: aCanvas y: unaRespuesta
	aCanvas
		listItem: [ aCanvas text: 'Respuesta de '.
			aCanvas anchor
				callback: [ self verPrefilDeUsuario: unaRespuesta usuario ];
				with: unaRespuesta usuario miNombre.
			unaRespuesta usuario miNombre.
			aCanvas paragraph: unaRespuesta contenido.
			aCanvas
				paragraph:
					unaRespuesta likes asString , ' likes | '
						, unaRespuesta dislikes asString , ' dislikes'.
			aCanvas anchor
				callback: [ self likeRespuesta: unaRespuesta ];
				with: 'like'.
			aCanvas space.
			aCanvas anchor
				callback: [ self dislikeRespuesta: unaRespuesta ];
				with: 'dislike' ].
	aCanvas horizontalRule