rendering
renderAgregarRespuestaOn: aCanvas
	aCanvas paragraph: 'No hay mas respuestas'.
	aCanvas anchor
		callback: [ self agregarRespuesta ];
		with: [ aCanvas button: 'Agregar Respuesta' ].