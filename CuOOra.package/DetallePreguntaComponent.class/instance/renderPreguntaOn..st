rendering
renderPreguntaOn: aCanvas
	aCanvas paragraph: pregunta titulo.
	pregunta  topicos do: [ :each | aCanvas text: each nombre ].
	aCanvas break.
	aCanvas text: 'Realizada por '.
	aCanvas anchor
		callback: [ self verPrefilDeUsuario: pregunta usuario ];
		with: pregunta usuario miNombre.
	aCanvas
		paragraph:
			pregunta likes asString , ' likes | '
				, pregunta dislikes asString , ' dislikes'.
	aCanvas anchor
		callback: [ self likePregunta: pregunta ];
		with: 'like'.
	aCanvas space.
	aCanvas anchor
		callback: [ self dislikePregunta: pregunta ];
		with: 'dislike'.
	"preguntasRelevantesPara: self session usuario"