rendering
renderContentOn: aCanvas
	self renderEncabezadoOn: aCanvas.
	aCanvas horizontalRule.
	self renderPreguntaOn: aCanvas.
	aCanvas horizontalRule.
	self renderRespuestasOn: aCanvas.
	aCanvas horizontalRule.
	self renderAgregarRespuestaOn: aCanvas