rendering
renderPreguntasOn: aCanvas
	aCanvas paragraph with: 'Preguntas de ' , self perfil miNombre.
	aCanvas anchor
		callback: [ self follow ];
		with: 'follow'.
	aCanvas
		unorderedList: [ self perfil misPreguntas
				do: [ :each | self renderPreguntaOn: aCanvas y: each ] ]