rendering
renderPreguntaOn: aCanvas y: unaPregunta
	aCanvas
		listItem: [ aCanvas paragraph: unaPregunta titulo.
			unaPregunta topicos do: [ :each | aCanvas text: each nombre ].
			aCanvas break.
			aCanvas text: 'Realizada por ' , unaPregunta usuario miNombre ].
	aCanvas horizontalRule	"preguntasRelevantesPara: self session usuario"